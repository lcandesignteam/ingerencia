package com.example.inGerencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InGerenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(InGerenciaApplication.class, args);
	}

}
