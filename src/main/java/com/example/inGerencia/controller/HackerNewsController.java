package com.example.inGerencia.controller;

import com.example.inGerencia.configuration.ConfigLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class HackerNewsController
{
    @GetMapping("/ping")
    public String ping() {
        return "Service Online";
    }

    @GetMapping("/getNews")
    private String news() {
        String urlApi = ConfigLoader.getProperty("url.api");
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(urlApi, String.class);
        return result;
    }

}
