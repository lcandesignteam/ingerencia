package com.example.inGerencia.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {
    private static final Logger log = LogManager.getLogger(ConfigLoader.class);
    private static Properties props;

    static {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream stream = classLoader.getResourceAsStream("application.properties");
            ConfigLoader.props = new Properties();
            props.load(stream);
        } catch (IOException e) {
            log.error("Ocurrió un error al cargar las configuraciones");
            e.printStackTrace();
        }
    }

    public static String getProperty(String propertyName) {
        return props.getProperty(propertyName);
    }
}
