# Full Stack Developer Test

Se solicita la creación de una API REST que sirva de Adapter entre el cliente y una API pública,
esta última puede estar definida a través de las propiedades que configure el usuario.

## Ejecución del proyecto paso a paso
1. ***Clonar el repositorio***
```bash
git clone https://USER@bitbucket.org/lcandesignteam/ingerencia.git
```

*reemplace **USER*** por su usuario de bitbucket

----
2. **Configuración**

La configuración inicial la encontrará en el archivo `src/main/resources/application.properties` en este puede modificar el puerto de arranque de la aplicación y la url de la API por defecto viene de la siguiente forma: 
```properties
server.port = 8090
url.api = https://hn.algolia.com/api/v1/search_by_date?query=java
spring.mvc.pathmatch.matching-strategy = ANT_PATH_MATCHER
```

----
3. ***Run API***

Ejecute la clase `com/example/inGerencia/InGerenciaApplication`

---
4. ***Comprobación***
Si usted no modifico el archivo de configuración la aplicación por defecto se ejecutara en la URL:
`localhost:8090/api/`
---

## Información Adicional
Si desea probar los métodos de la aplicación puede hacerlo a través de la documentación correspondiente, la cual la encontrara en la URL:
`http://localhost:8090/swagger-ui.html`. En esta puede ejecutar los métodos:

* `ping`: para validar si el servicio está activo.
* `getNews`: que devuelve las noticias de la API indicada.